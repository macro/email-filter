#email filter

####功能描述列表

<table>
    <tr>
        <th>文件名</th>
        <th>功能说明</th>
        <th>备注</th>
    </tr>
    <tr>
        <td>packages.lisp</td>
        <td>处理路径</td>
        <td>暂无</td>
    </tr>
    <tr>
        <td>filter.lisp</td>
        <td>垃圾邮件特征提取和分类</td>
        <td>暂无</td>
    </tr>
</table>

* 加载:
    <pre><code>(load "your directory/filter.lisp")</code></pre>
    <pre><code>(in-package :com.happinesscoast.spam)</code></pre>
* 测试特征提取器:
    <pre><code>(extract-word "make money fast")</code></pre>

###有时间再做补充！